# StreamのコネクションをCloseしたときの動き

### すぐわすれるprotocコマンド
`protoc --go_out=plugins=grpc:proto proto/close-connection.proto -I proto/`

### すぐわすれるgo getコマンド
`go get google.golang.org/grpc`  
`go get google.golang.org/genproto`
