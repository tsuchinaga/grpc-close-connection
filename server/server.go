package main

import (
	proto "gitlab.com/tsuchinaga/grpc-close-connection/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

type server struct {
	proto.ConnectionServer
}

func (s *server) Receive(_ *proto.ReceiveRequest, stream proto.Connection_ReceiveServer) error {
	for {
		time.Sleep(3 * time.Second)
		if err := stream.Send(&proto.Message{Message: "hogehoge"}); err != nil {
			log.Println(err)
			break
		}
	}

	return nil
}

func main() {
	lis, err := net.Listen("tcp", ":5432")
	if err != nil {
		log.Fatalf("failed to listen: %+v\n", err)
	}

	s := grpc.NewServer()
	proto.RegisterConnectionServer(s, new(server))
	if err = s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %+v\n", err)
	}
}
