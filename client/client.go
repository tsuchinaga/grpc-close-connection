package main

import (
	"context"
	proto "gitlab.com/tsuchinaga/grpc-close-connection/proto"
	"google.golang.org/grpc"
	"io"
	"log"
	"time"
)

func main() {
	conn, err := grpc.Dial("localhost:5432", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %+v\n", err)
	}
	defer conn.Close()

	c := proto.NewConnectionClient(conn)
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	stream, err := c.Receive(ctx, new(proto.ReceiveRequest))
	if err != nil {
		log.Fatalln(err)
	}

	go func() {
		time.Sleep(10 * time.Second)
		log.Println("切断開始")
		cancel()
		log.Println("切断終了")
	}()

	for {
		resp, err := stream.Recv()
		if err == io.EOF {
			log.Println("streamが切れました")
			break
		} else if err != nil {
			if ctx.Err() == context.Canceled {
				log.Println("streamがキャンセルされました")
				break
			}
			log.Fatalln(err)
		}
		log.Println(resp)
	}
}
