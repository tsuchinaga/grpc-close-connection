module gitlab.com/tsuchinaga/grpc-close-connection

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	google.golang.org/appengine v1.4.0 // indirect
	google.golang.org/genproto v0.0.0-20190620144150-6af8c5fc6601 // indirect
	google.golang.org/grpc v1.21.1
)
